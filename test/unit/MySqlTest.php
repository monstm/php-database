<?php

namespace Test\Unit;

use mysqli_result;
use Samy\Database\MySql;
use Samy\Database\DataTransferObject\MySqlDTO;

class MySqlTest extends AbstractTestCase
{
    protected function setUp(): void
    {
        $this->mock["database"] = $this->createMock(MySqlDTO::class);
        $this->mock["mysqli_result"] = $this->createMock(mysqli_result::class);

        $config = [
            /** @phpstan-ignore-next-line */
            "host" => MYSQL_HOST,
            /** @phpstan-ignore-next-line */
            "username" => MYSQL_USERNAME,
            /** @phpstan-ignore-next-line */
            "password" => MYSQL_PASSWORD,
            /** @phpstan-ignore-next-line */
            "database" => MYSQL_DATABASE,
            /** @phpstan-ignore-next-line */
            "port" => intval(MYSQL_PORT)
        ];

        /** @phpstan-ignore-next-line */
        if (MOCK_TEST) {
            $filelist = dirname(__DIR__) .
                DIRECTORY_SEPARATOR . "lst" .
                DIRECTORY_SEPARATOR . "mysql-function.lst";

            $config["_database"] = $this->mock["database"];
            $config["_functions"] = $this->lst($filelist);
        }

        $this->database = new MySql($config);
    }

    /**
     * Test escape.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataEscape
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testEscape(array $Data): void
    {
        $this->mockDataProvider($Data);

        $plaintext = $this->getStringDataProvider($Data, "plaintext");
        $this->assertDataProvider($Data, "expect", $this->database->escape($plaintext));
    }

    /**
     * Test function.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataFunction
     * @param bool $Expect
     * @param string $Expresion
     * @return void
     */
    public function testFunction(bool $Expect, string $Expresion): void
    {
        $this->assertSame($Expect, $this->database->isFunction($Expresion));
    }

    /**
     * Test execute.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataExecute
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testExecute(array $Data): void
    {
        $this->mockDataProvider($Data);

        $execute = $this->getStringDataProvider($Data, "execute");
        $this->assertInstanceOf(MySql::class, $this->database->execute($execute));
        $this->assertDataProvider($Data, "insert_id", $this->database->getInsertId());
        $this->assertDataProvider($Data, "affected_rows", $this->database->getAffectedRows());
        $this->assertSame($execute, $this->database->getExecuted());
    }

    /**
     * Test query.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataQuery
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testQuery(array $Data): void
    {
        $this->mockDataProvider($Data);

        $query = $this->getStringDataProvider($Data, "query");
        $this->assertDataProvider($Data, "result", $this->database->query($query));
        $this->assertSame($query, $this->database->getQueried());
    }

    /**
     * Test select.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataSelect
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testSelect(array $Data): void
    {
        $this->mockDataProvider($Data);

        $select = $this->getArrayDataProvider($Data, "data");
        $this->assertDataProvider($Data, "result", $this->database->select($select));
    }

    /**
     * Test record.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataRecord
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testRecord(array $Data): void
    {
        $this->mockDataProvider($Data);

        $record = $this->getArrayDataProvider($Data, "data");
        $this->assertDataProvider($Data, "result", $this->database->record($record));
    }

    /**
     * Test insert.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataInsert
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testInsert(array $Data): void
    {
        $this->mockDataProvider($Data);

        $insert = $this->getArrayDataProvider($Data, "data");
        $this->assertInstanceOf(MySql::class, $this->database->insert($insert));
    }

    /**
     * Test update.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataUpdate
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testUpdate(array $Data): void
    {
        $this->mockDataProvider($Data);

        $update = $this->getArrayDataProvider($Data, "data");
        $this->assertInstanceOf(MySql::class, $this->database->update($update));
    }

    /**
     * Test delete.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataDelete
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testDelete(array $Data): void
    {
        $this->mockDataProvider($Data);

        $delete = $this->getArrayDataProvider($Data, "data");
        $this->assertInstanceOf(MySql::class, $this->database->delete($delete));
    }

    /**
     * Test count.
     *
     * @dataProvider \Test\DataProvider\MySqlDataProvider::dataCount
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testCount(array $Data): void
    {
        $this->mockDataProvider($Data);

        $count = $this->getArrayDataProvider($Data, "data");
        $this->assertDataProvider($Data, "result", $this->database->count($count));
    }
}
