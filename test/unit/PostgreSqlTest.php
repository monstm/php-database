<?php

namespace Test\Unit;

use Samy\Database\PostgreSql;
use Samy\Database\DataTransferObject\PostgreSqlDTO;
use Samy\Database\DataTransferObject\PostgreSqlResultDTO;

class PostgreSqlTest extends AbstractTestCase
{
    protected function setUp(): void
    {
        $this->mock["database"] = $this->createMock(PostgreSqlDTO::class);
        $this->mock["PostgreSqlResultDTO"] = $this->createMock(PostgreSqlResultDTO::class);

        $config = [
            /** @phpstan-ignore-next-line */
            "host" => POSTGRESQL_HOST,
            /** @phpstan-ignore-next-line */
            "username" => POSTGRESQL_USERNAME,
            /** @phpstan-ignore-next-line */
            "password" => POSTGRESQL_PASSWORD,
            /** @phpstan-ignore-next-line */
            "database" => POSTGRESQL_DATABASE,
            /** @phpstan-ignore-next-line */
            "port" => intval(POSTGRESQL_PORT)
        ];

        /** @phpstan-ignore-next-line */
        if (MOCK_TEST) {
            $filelist = dirname(__DIR__) .
                DIRECTORY_SEPARATOR . "lst" .
                DIRECTORY_SEPARATOR . "postgresql-function.lst";

            $config["_database"] = $this->mock["database"];
            $config["_functions"] = $this->lst($filelist);
        }

        $this->database = new PostgreSql($config);
    }

    /**
     * Test escape.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataEscape
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testEscape(array $Data): void
    {
        $this->mockDataProvider($Data);

        $plaintext = $this->getStringDataProvider($Data, "plaintext");
        $this->assertDataProvider($Data, "expect", $this->database->escape($plaintext));
    }

    /**
     * Test function.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataFunction
     * @param bool $Expect
     * @param string $Expresion
     * @return void
     */
    public function testFunction(bool $Expect, string $Expresion): void
    {
        $this->assertSame($Expect, $this->database->isFunction($Expresion));
    }

    /**
     * Test execute.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataExecute
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testExecute(array $Data): void
    {
        $this->mockDataProvider($Data);

        $execute = $this->getStringDataProvider($Data, "execute");
        $this->assertInstanceOf(PostgreSql::class, $this->database->execute($execute));
        $this->assertDataProvider($Data, "insert_id", $this->database->getInsertId());
        $this->assertDataProvider($Data, "affected_rows", $this->database->getAffectedRows());
        $this->assertSame($execute, $this->database->getExecuted());
    }

    /**
     * Test query.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataQuery
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testQuery(array $Data): void
    {
        $this->mockDataProvider($Data);

        $query = $this->getStringDataProvider($Data, "query");
        $this->assertDataProvider($Data, "result", $this->database->query($query));
        $this->assertSame($query, $this->database->getQueried());
    }

    /**
     * Test select.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataSelect
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testSelect(array $Data): void
    {
        $this->mockDataProvider($Data);

        $select = $this->getArrayDataProvider($Data, "data");
        $this->assertDataProvider($Data, "result", $this->database->select($select));
    }

    /**
     * Test record.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataRecord
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testRecord(array $Data): void
    {
        $this->mockDataProvider($Data);

        $record = $this->getArrayDataProvider($Data, "data");
        $this->assertDataProvider($Data, "result", $this->database->record($record));
    }

    /**
     * Test insert.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataInsert
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testInsert(array $Data): void
    {
        $this->mockDataProvider($Data);

        $insert = $this->getArrayDataProvider($Data, "data");
        $this->assertInstanceOf(PostgreSql::class, $this->database->insert($insert));
    }

    /**
     * Test update.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataUpdate
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testUpdate(array $Data): void
    {
        $this->mockDataProvider($Data);

        $update = $this->getArrayDataProvider($Data, "data");
        $this->assertInstanceOf(PostgreSql::class, $this->database->update($update));
    }

    /**
     * Test delete.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataDelete
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testDelete(array $Data): void
    {
        $this->mockDataProvider($Data);

        $delete = $this->getArrayDataProvider($Data, "data");
        $this->assertInstanceOf(PostgreSql::class, $this->database->delete($delete));
    }

    /**
     * Test count.
     *
     * @dataProvider \Test\DataProvider\PostgreSqlDataProvider::dataCount
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testCount(array $Data): void
    {
        $this->mockDataProvider($Data);

        $count = $this->getArrayDataProvider($Data, "data");
        $this->assertDataProvider($Data, "result", $this->database->count($count));
    }
}
