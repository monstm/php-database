<?php

namespace Test\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use Samy\Database\MySql;
use Samy\Database\PostgreSql;
use Samy\PhpUnit\AbstractTestCase as PhpUnitAbstractTestCase;

class AbstractTestCase extends PhpUnitAbstractTestCase
{
    /** @var array<string,MockObject> */
    protected $mock = [];

    /** @var MySql|PostgreSql */
    protected $database = null;

    /**
     * Mock data provider.
     *
     * @param array<string,mixed> $Data
     * @return void
     */
    protected function mockDataProvider(array $Data): void
    {
        $mock = $Data["mock"] ?? null;
        if (!is_array($mock)) {
            return;
        }

        foreach ($mock as $name => $method) {
            if (!isset($this->mock[$name])) {
                continue;
            }

            if (!is_array($method)) {
                continue;
            }

            $this->mockMethod($name, $method);
        }
    }

    /**
     * Mock method.
     *
     * @param string $MockName
     * @param array<string,mixed> $Data
     * @return void
     */
    private function mockMethod(string $MockName, array $Data): void
    {
        foreach ($Data as $name => $value) {
            if (!is_array($value)) {
                continue;
            }

            $method = $this->mock[$MockName]->method($name);

            if (array_key_exists("mock", $value)) {
                $mock = $value["mock"];
                $method->willReturn($this->mock[$mock]);
            }

            if (array_key_exists("return", $value)) {
                $method->willReturn($value["return"]);
            }

            if (array_key_exists("consecutive", $value)) {
                call_user_func_array(array($method, "willReturnOnConsecutiveCalls"), $value["consecutive"]);
            }

            if (array_key_exists("exception", $value)) {
                $method->willThrowException($value["exception"]);
            }
        }
    }

    /**
     * Assert data provider.
     *
     * @param array<string,mixed> $Data
     * @param string $Key
     * @param mixed $Actual
     * @return void
     */
    protected function assertDataProvider(array $Data, string $Key, mixed $Actual): void
    {
        $this->assertSame($Data[$Key] ?? null, $Actual);
    }

    /**
     * Retrieve string from data provider.
     *
     * @param array<string,mixed> $Data
     * @param string $Key
     * @return string
     */
    protected function getStringDataProvider(array $Data, string $Key): string
    {
        return strval($Data[$Key] ?? "");
    }

    /**
     * Retrieve array from data provider.
     *
     * @param array<string,mixed> $Data
     * @param string $Key
     * @return array<string,mixed>
     */
    protected function getArrayDataProvider(array $Data, string $Key): array
    {
        return isset($Data[$Key]) && is_array($Data[$Key]) ? $Data[$Key] : [];
    }
}
