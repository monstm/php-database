<?php

namespace Test\DataProvider;

class MySqlDataProvider extends AbstractDataProvider
{
    /** @var string */
    protected static $json_unittest = "mysql";

    /** @var string */
    protected static $list_function = "mysql-function";
}
