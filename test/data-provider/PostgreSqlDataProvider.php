<?php

namespace Test\DataProvider;

class PostgreSqlDataProvider extends AbstractDataProvider
{
    /** @var string */
    protected static $json_unittest = "postgresql";

    /** @var string */
    protected static $list_function = "postgresql-function";
}
