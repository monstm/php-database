<?php

namespace Test\DataProvider;

use Samy\PhpUnit\AbstractDataProvider as PhpUnitAbstractDataProvider;

abstract class AbstractDataProvider extends PhpUnitAbstractDataProvider
{
    /** @var string */
    protected static $json_unittest = "";

    /** @var string */
    protected static $list_function = "";

    /**
     * Retrieve JSon data provider.
     *
     * @param string $Name
     * @param string $Key
     * @return array<mixed>
     */
    protected static function jsonData(string $Name, string $Key): array
    {
        $filename = dirname(__DIR__) .
            DIRECTORY_SEPARATOR . "json" .
            DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".json";
        $json = self::json($filename);

        return is_array($json[$Key]) ? $json[$Key] : [];
    }

    /**
     * Retrieve list data provider.
     *
     * @param string $Name
     * @return array<string>
     */
    protected static function listData(string $Name): array
    {
        $filename = dirname(__DIR__) .
            DIRECTORY_SEPARATOR . "lst" .
            DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".lst";

        return self::lst($filename);
    }

    /**
     * Retrieve escape data provider.
     *
     * @return array<mixed>
     */
    public static function dataEscape(): array
    {
        return self::jsonData(static::$json_unittest, "escape");
    }

    /**
     * Retrieve function data provider.
     *
     * @return array<mixed>
     */
    public static function dataFunction(): array
    {
        $ret = []; // expect, exporesion

        foreach (self::listData(static::$list_function) as $index => $value) {
            $line = $index + 1;
            $ret["function-" . $line . "-true"] = [true, $value . "()"];
            $ret["function-" . $line . "-false"] = [false, $value . "_FAKE()"];
        }

        return $ret;
    }

    /**
     * Retrieve execute data provider.
     *
     * @return array<mixed>
     */
    public static function dataExecute(): array
    {
        return self::jsonData(static::$json_unittest, "execute");
    }

    /**
     * Retrieve query data provider.
     *
     * @return array<mixed>
     */
    public static function dataQuery(): array
    {
        return self::jsonData(static::$json_unittest, "query");
    }

    /**
     * Retrieve select data provider.
     *
     * @return array<mixed>
     */
    public static function dataSelect(): array
    {
        return self::jsonData(static::$json_unittest, "select");
    }

    /**
     * Retrieve record data provider.
     *
     * @return array<mixed>
     */
    public static function dataRecord(): array
    {
        return self::jsonData(static::$json_unittest, "record");
    }

    /**
     * Retrieve insert data provider.
     *
     * @return array<mixed>
     */
    public static function dataInsert(): array
    {
        return self::jsonData(static::$json_unittest, "insert");
    }

    /**
     * Retrieve update data provider.
     *
     * @return array<mixed>
     */
    public static function dataUpdate(): array
    {
        return self::jsonData(static::$json_unittest, "update");
    }

    /**
     * Retrieve delete data provider.
     *
     * @return array<mixed>
     */
    public static function dataDelete(): array
    {
        return self::jsonData(static::$json_unittest, "delete");
    }

    /**
     * Retrieve count data provider.
     *
     * @return array<mixed>
     */
    public static function dataCount(): array
    {
        return self::jsonData(static::$json_unittest, "count");
    }
}
