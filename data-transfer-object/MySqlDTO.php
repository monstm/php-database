<?php

namespace Samy\Database\DataTransferObject;

use mysqli;
use mysqli_result;
use Samy\Database\DatabaseException;
use Samy\Validation\ValidationException;

/**
 * MySQL Data Transfer Object
 */
class MySqlDTO
{
    /** @var mysqli */
    private $database = null;

    /**
     * @param array<string,mixed> $Data The database configuration.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Data)
    {
        $config = $this->getConfig($Data);
        $this->database = $this->getDatabase($config);
    }

    public function __destruct()
    {
        if ($this->database instanceof mysqli) {
            $this->database->close();
        }
    }

    /**
     * Retrieve configuration data transfer object.
     *
     * @param array<string,mixed> $Data The data configuration.
     * @throws ValidationException If invalid.
     * @return ConfigDTO
     */
    private function getConfig(array $Data): ConfigDTO
    {
        return new ConfigDTO($Data, [
            "host" => "localhost",
            "username" => "root",
            "password" => "",
            "database" => "",
            "port" => 3306
        ]);
    }

    /**
     * Retrieve database connection.
     *
     * @param ConfigDTO $Config The data configuration.
     * @throws DatabaseException If error.
     * @return mysqli
     */
    private function getDatabase(ConfigDTO $Config): mysqli
    {
        mysqli_report(MYSQLI_REPORT_OFF);

        $ret = @new mysqli(
            $Config->host(),
            $Config->username(),
            $Config->password(),
            $Config->database(),
            $Config->port()
        );

        if ($ret->connect_errno) {
            throw new DatabaseException($ret->connect_error ?? "", $ret->connect_errno);
        }

        return $ret;
    }

    /**
     * Returns the error code for the most recent function call.
     *
     * @return int
     */
    public function errno(): int
    {
        return $this->database->errno;
    }

    /**
     * Returns a string description of the last error.
     *
     * @return string
     */
    public function error(): string
    {
        return $this->database->error;
    }

    /**
     * Returns the value generated for an AUTO_INCREMENT column by the last query.
     *
     * @return int
     */
    public function insertId(): int
    {
        return intval($this->database->insert_id);
    }

    /**
     * Gets the number of affected rows in a previous MySQL operation.
     *
     * @return int
     */
    public function affectedRows(): int
    {
        return intval($this->database->affected_rows);
    }

    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * @param string $string
     * @return string
     */
    public function realEscapeString(string $string): string
    {
        return $this->database->real_escape_string($string);
    }

    /**
     * Performs a query on the database.
     *
     * @param string $query
     * @param int $result_mode
     * @return mysqli_result|bool
     */
    public function query(string $query, int $result_mode = MYSQLI_STORE_RESULT): mysqli_result|bool
    {
        return $this->database->query($query, $result_mode);
    }
}
