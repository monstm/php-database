<?php

namespace Samy\Database\DataTransferObject;

use PgSql\Result;

/**
 * PostgreSQL Result Data Transfer Object
 */
class PostgreSqlResultDTO
{
    /** @var Result */
    private $result = null;

    /**
     * @param Result $Result The postgresql result.
     */
    public function __construct(Result $Result)
    {
        $this->result = $Result;
    }

    /**
     * Retrieve query error code.
     *
     * @return int
     */
    public function errorCode(): int
    {
        $status = pg_result_status($this->result);
        $ret = in_array($status, [PGSQL_COMMAND_OK, PGSQL_TUPLES_OK]) ? 0 : $status;

        return is_int($ret) ? $ret : PGSQL_FATAL_ERROR;
    }

    /**
     * Retrieve query error message.
     *
     * @return string
     */
    public function errorMessage(): string
    {
        return strval(pg_result_error($this->result));
    }

    /**
     * Get a row as an enumerated array.
     *
     * @param int $Row Row number in result to fetch.
     * @param int $Mode An optional parameter that controls how the returned array is indexed.
     * @return array<mixed>|false
     */
    public function fetchRow(?int $Row = null, int $Mode = PGSQL_NUM): array|false
    {
        return pg_fetch_row($this->result, $Row, $Mode);
    }

    /**
     * Returns the value generated for an RETURNING column by the last query.
     *
     * @return int
     */
    public function insertId(): int
    {
        $ret = 0;
        $fetch_row = $this->fetchRow(null, PGSQL_NUM);

        if (is_array($fetch_row)) {
            foreach ($fetch_row as $value) {
                $lastval = intval($value);
                if (($ret == 0) && ($lastval > 0)) {
                    $ret = $lastval;
                }
            }
        }

        return $ret;
    }

    /**
     * Returns number of affected records (tuples).
     *
     * @return int
     */
    public function affectedRows(): int
    {
        return pg_affected_rows($this->result);
    }

    /**
     * Returns the number of fields in a result.
     *
     * @return int
     */
    public function numFields(): int
    {
        return pg_num_fields($this->result);
    }

    /**
     * Returns the name of a field.
     *
     * @param int $Index Field number, starting from 0.
     * @return string
     */
    public function fieldName(int $Index): string
    {
        return pg_field_name($this->result, $Index);
    }

    /**
     * Returns the type name for the corresponding field number.
     *
     * @param int $Index Field number, starting from 0.
     * @return string
     */
    public function fieldType(int $Index): string
    {
        return pg_field_type($this->result, $Index);
    }
}
