<?php

namespace Samy\Database\DataTransferObject;

use PgSql\Connection;
use PgSql\Result;
use Samy\Database\DatabaseException;
use Samy\Validation\ValidationException;

/**
 * PostgreSQL Data Transfer Object
 */
class PostgreSqlDTO
{
    /** @var Connection */
    private $database = null;

    /** @var string */
    private $database_name = "";

    /**
     * @param array<string,mixed> $Data The database configuration.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Data)
    {
        $config = $this->getConfig($Data);
        $this->database = $this->getDatabase($config);
    }

    public function __destruct()
    {
        if ($this->database instanceof Connection) {
            pg_close($this->database);
        }
    }

    /**
     * Retrieve configuration data transfer object.
     *
     * @param array<string,mixed> $Data The data configuration.
     * @throws ValidationException If invalid.
     * @return ConfigDTO
     */
    private function getConfig(array $Data): ConfigDTO
    {
        return new ConfigDTO($Data, [
            "host" => "localhost",
            "username" => "postgres",
            "password" => "postgres",
            "database" => "",
            "port" => 5432
        ]);
    }

    /**
     * Retrieve database connection.
     *
     * @param ConfigDTO $Config The data configuration.
     * @throws DatabaseException If error.
     * @return Connection
     */
    private function getDatabase(ConfigDTO $Config): Connection
    {
        $port = strval($Config->port());

        $ret = @pg_pconnect(
            "host=" . $Config->host() .
                " user=" . $Config->username() .
                " password=" . $Config->password() .
                " dbname=" . $Config->database() .
                " port=" . $port
        );

        if (!$ret instanceof Connection) {
            $message = "Unable to open database - " .
                $Config->username() . "@" . $Config->host() .
                ":" . $port . "/" . $Config->database();

            throw new DatabaseException($message);
        }

        $this->database_name = $Config->database();
        return $ret;
    }

    /**
     * @return string
     */
    public function databaseName(): string
    {
        return $this->database_name;
    }

    /**
     * Get the last error message string of a connection.
     *
     * @return string
     */
    public function lastError(): string
    {
        return pg_last_error($this->database);
    }

    /**
     * Escape a identifier for insertion into a text field.
     *
     * @param string $string
     * @return string
     */
    public function escapeIdentifier(string $string): string
    {
        $ret = pg_escape_identifier($this->database, $string);
        return is_string($ret) ? $ret : "";
    }

    /**
     * Escape a string for query.
     *
     * @param string $string
     * @return string
     */
    public function escapeString(string $string): string
    {
        return pg_escape_string($this->database, $string);
    }

    /**
     * Execute a query.
     *
     * @param string $query
     * @return PostgreSqlResultDTO|bool
     */
    public function query(string $query): PostgreSqlResultDTO|bool
    {
        $ret = pg_query($this->database, $query);
        return ($ret instanceof Result) ? new PostgreSqlResultDTO($ret) : $ret;
    }
}
