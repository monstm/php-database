<?php

namespace Samy\Database\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 * Update Data Transfer Object
 */
class UpdateDTO
{
    /** @var array<string> */
    private $table = [];

    /** @var array<string,mixed> */
    private $data = [];

    /** @var array<mixed> */
    private $filter = [];

    /** @var int */
    private $limit = 0;

    /** @var bool */
    private $sensitive = false;

    /**
     * @param array<string,mixed> $Data The transfer data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Data)
    {
        $validation = new Validation();
        $validation
            ->withRule("table", ["required" => true, "type" => "string"])
            ->withRule("data", ["required" => true, "type" => "array"])
            ->withRule("filter", ["type" => "array"])
            ->withRule("limit", ["type" => "integer"])
            ->withRule("sensitive", ["type" => "boolean"])
            ->validate($Data);

        $this->table = is_array($Data["table"]) ? $Data["table"] : [$Data["table"]];
        $this->data = is_array($Data["data"]) ? $Data["data"] : [];
        $this->filter = isset($Data["filter"]) && is_array($Data["filter"]) ? $Data["filter"] : [];
        $this->limit = isset($Data["limit"]) && is_int($Data["limit"]) ? $Data["limit"] : 0;
        $this->sensitive = isset($Data["sensitive"]) && is_bool($Data["sensitive"]) ? $Data["sensitive"] : false;
    }

    /**
     * @return array<string>
     */
    public function table(): array
    {
        return $this->table;
    }

    /**
     * @return array<string,mixed>
     */
    public function data(): array
    {
        return $this->data;
    }

    /**
     * @return array<mixed>
     */
    public function filter(): array
    {
        return $this->filter;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function sensitive(): bool
    {
        return $this->sensitive;
    }
}
