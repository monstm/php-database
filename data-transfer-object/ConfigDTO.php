<?php

namespace Samy\Database\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 * Config Data Transfer Object
 */
class ConfigDTO
{
    /** @var string */
    private $host = "";

    /** @var string */
    private $username = "";

    /** @var string */
    private $password = "";

    /** @var string */
    private $database = "";

    /** @var int */
    private $port = 0;

    /**
     * @param array<string,mixed> $Config The config data.
     * @param array<string,mixed> $Default The default data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Config, array $Default)
    {
        $data = array_merge($Default, $Config);

        $validation = new Validation();
        $validation
            ->withRule("host", ["type" => "string"])
            ->withRule("username", ["type" => "string"])
            ->withRule("password", ["type" => "string"])
            ->withRule("database", ["type" => "string"])
            ->withRule("port", ["type" => "integer"])
            ->validate($data);

        $this->host = is_string($data["host"]) ? $data["host"] : "";
        $this->username = is_string($data["username"]) ? $data["username"] : "";
        $this->password = is_string($data["password"]) ? $data["password"] : "";
        $this->database = is_string($data["database"]) ? $data["database"] : "";
        $this->port = is_int($data["port"]) ? $data["port"] : 0;
    }

    /**
     * @return string
     */
    public function host(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function database(): string
    {
        return $this->database;
    }

    /**
     * @return int
     */
    public function port(): int
    {
        return $this->port;
    }
}
