<?php

namespace Samy\Database\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 * Read Data Transfer Object
 */
class ReadDTO
{
    /** @var array<string> */
    private $table = [];

    /** @var array<int|string,string> */
    private $field = [];

    /** @var array<mixed> */
    private $filter = [];

    /** @var array<string> */
    private $group = [];

    /** @var array<string,string> */
    private $order = [];

    /** @var int */
    private $limit = 0;

    /** @var int */
    private $offset = 0;

    /** @var bool */
    private $sensitive = false;

    /**
     * @param array<string,mixed> $Data The transfer data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Data)
    {
        $validation = new Validation();
        $validation
            ->withRule("table", ["required" => true, "type" => "string|array"])
            ->withRule("field", ["required" => true, "type" => "array"])
            ->withRule("filter", ["type" => "array"])
            ->withRule("group", ["type" => "array"])
            ->withRule("order", ["type" => "array"])
            ->withRule("limit", ["type" => "integer"])
            ->withRule("offset", ["type" => "integer"])
            ->withRule("sensitive", ["type" => "boolean"])
            ->validate($Data);

        $this->table = is_array($Data["table"]) ? $Data["table"] : [$Data["table"]];
        $this->field = is_array($Data["field"]) ? $Data["field"] : [];
        $this->filter = isset($Data["filter"]) && is_array($Data["filter"]) ? $Data["filter"] : [];
        $this->group = isset($Data["group"]) && is_array($Data["group"]) ? $Data["group"] : [];
        $this->order = isset($Data["order"]) && is_array($Data["order"]) ? $Data["order"] : [];
        $this->limit = isset($Data["limit"]) && is_int($Data["limit"]) ? $Data["limit"] : 0;
        $this->offset = isset($Data["offset"]) && is_int($Data["offset"]) ? $Data["offset"] : 0;
        $this->sensitive = isset($Data["sensitive"]) && is_bool($Data["sensitive"]) ? $Data["sensitive"] : false;
    }

    /**
     * @return array<string>
     */
    public function table(): array
    {
        return $this->table;
    }

    /**
     * @return array<int|string,string>
     */
    public function field(): array
    {
        return $this->field;
    }

    /**
     * @return array<mixed>
     */
    public function filter(): array
    {
        return $this->filter;
    }

    /**
     * @return array<string>
     */
    public function group(): array
    {
        return $this->group;
    }

    /**
     * @return array<string,string>
     */
    public function order(): array
    {
        return $this->order;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function offset(): int
    {
        return $this->offset;
    }

    /**
     * @return bool
     */
    public function sensitive(): bool
    {
        return $this->sensitive;
    }
}
