<?php

namespace Samy\Database\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 * Create Data Transfer Object
 */
class CreateDTO
{
    /** @var array<string> */
    private $table = [];

    /** @var array<string,mixed> */
    private $data = [];

    /**
     * @param array<string,mixed> $Data The transfer data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Data)
    {
        $validation = new Validation();
        $validation
            ->withRule("table", ["required" => true, "type" => "string"])
            ->withRule("data", ["required" => true, "type" => "array"])
            ->validate($Data);

        $this->table = is_array($Data["table"]) ? $Data["table"] : [$Data["table"]];
        $this->data = is_array($Data["data"]) ? $Data["data"] : [];
    }

    /**
     * @return array<string>
     */
    public function table(): array
    {
        return $this->table;
    }

    /**
     * @return array<string,mixed>
     */
    public function data(): array
    {
        return $this->data;
    }
}
