<?php

namespace Samy\Database\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 * Count Data Transfer Object
 */
class CountDTO
{
    /** @var array<string> */
    private $table = [];

    /** @var array<mixed> */
    private $filter = [];

    /** @var bool */
    private $sensitive = false;

    /**
     * @param array<string,mixed> $Data The transfer data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Data)
    {
        $validation = new Validation();
        $validation
            ->withRule("table", ["required" => true, "type" => "string"])
            ->withRule("filter", ["type" => "array"])
            ->withRule("sensitive", ["type" => "boolean"])
            ->validate($Data);

        $this->table = is_array($Data["table"]) ? $Data["table"] : [$Data["table"]];
        $this->filter = isset($Data["filter"]) && is_array($Data["filter"]) ? $Data["filter"] : [];
        $this->sensitive = isset($Data["sensitive"]) && is_bool($Data["sensitive"]) ? $Data["sensitive"] : false;
    }

    /**
     * @return array<string>
     */
    public function table(): array
    {
        return $this->table;
    }

    /**
     * @return array<mixed>
     */
    public function filter(): array
    {
        return $this->filter;
    }

    /**
     * @return bool
     */
    public function sensitive(): bool
    {
        return $this->sensitive;
    }
}
