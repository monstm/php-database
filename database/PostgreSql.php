<?php

namespace Samy\Database;

use Samy\Database\Abstract\AbstractDatabase;
use Samy\Database\Constant\DatabaseFieldType;
use Samy\Database\DataTransferObject\PostgreSqlDTO;
use Samy\Database\DataTransferObject\PostgreSqlResultDTO;
use Samy\Validation\ValidationException;

/**
 * Simple PostgreSQL implementation.
 */
class PostgreSql extends AbstractDatabase
{
    protected $field_types = [
        DatabaseFieldType::INTEGER => [
            "int2", "int4", "int8",
            "bool"
        ],
        DatabaseFieldType::FLOAT => [
            "numeric",
            "float4", "float8",
            "money"
        ],
        DatabaseFieldType::STRING => [
            "bpchar", "varchar", "text",
            "tsquery", "tsvector",
            "uuid",
            "xml",
            "json", "jsonb",
            "name"
        ],
        DatabaseFieldType::TIME => [
            "date", "time",
            "timestamp", "timestamptz",
            "interval"
        ]
    ];

    /**
     * Retrieve postgresql database.
     *
     * @param array<string,mixed> $Data The data configuration.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return PostgreSqlDTO
     */
    protected function databaseFactory(array $Data): PostgreSqlDTO
    {
        return new PostgreSqlDTO($Data);
    }

    /**
     * Retrieve postgresql functions.
     *
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<string>
     */
    protected function functionsFactory(): array
    {
        $ret = ["IF"];
        $functions = array_map(
            function ($Data) {
                $routine_name = (is_string($Data["routine_name"]) ? $Data["routine_name"] : "");
                $function = html_entity_decode(trim($routine_name));
                $alphabet = preg_match("/[a-z]/i", $function);
                $no_space = !str_contains($function, " ");

                return ($alphabet && $no_space ? strtoupper($function) : "");
            },
            $this->query(
                "SELECT DISTINCT \"routine_name\" " .
                    "FROM \"information_schema\".\"routines\" " .
                    "WHERE " .
                    "(\"routine_catalog\" = '" . $this->database->databaseName() . "') AND " .
                    "(\"routine_type\" = 'FUNCTION')"
            )
        );

        foreach ($functions as $function) {
            if ($function == "") {
                continue;
            }

            array_push($ret, $function);
        }

        return array_unique($ret);
    }

    /**
     * Retrieve identifier name.
     *
     * @param string $UnidentifiedName The unidentified name.
     * @return string
     */
    protected function getIdentifier(string $UnidentifiedName): string
    {
        return $this->database->escapeIdentifier($UnidentifiedName);
    }

    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * Escapes special characters in the unescaped_string,
     * taking into account the current character set of the connection
     * so that it is safe to place it in a execute() or query().
     * If binary data is to be inserted, this function must be used.
     *
     * This function must always (with few exceptions) be used to make data safe before sending a query.
     *
     * @param string $UnescapedString The unescaped string.
     * @return string
     */
    public function escape(string $UnescapedString): string
    {
        return $this->database->escapeString($UnescapedString);
    }

    /**
     * Check if data expression is sql function.
     *
     * @param string $Expression The sql expression.
     * @return bool
     */
    public function isFunction(string $Expression): bool
    {
        $ret = false;
        $trim = trim($Expression);

        if (($trim != "") && (substr($trim, -1) == ")")) {
            $explode = explode("(", $trim);
            $ret = in_array(strtoupper($explode[0]), $this->functions);

            // special date operation
            if (!$ret && (count($explode) > 1) && str_contains(strtoupper($explode[1]), "INTERVAL")) {
                $ret = true;
            }
        }

        return $ret;
    }

    /**
     * Return an instance with executed query command.
     *
     * @param string $Command The sql command.
     * @throws DatabaseException If error.
     * @return static
     */
    public function execute(string $Command): self
    {
        $this->insert_id = 0;
        $this->affected_rows = 0;
        $this->executed = $Command;

        $result = $this->database->query($Command);
        if (!$result) {
            throw new DatabaseException($this->database->lastError());
        }

        $error_code = $result->errorCode();
        if ($error_code) {
            throw new DatabaseException($result->errorMessage(), $error_code);
        }

        $this->insert_id = $result->insertId();
        $this->affected_rows = $result->affectedRows();

        return $this;
    }

    /**
     * Retrieve records from query command.
     *
     * @param string $Command The sql command.
     * @throws DatabaseException If error.
     * @return array<array<string,mixed>>
     */
    public function query(string $Command): array
    {
        $ret = [];
        $this->queried = $Command;

        $result = $this->database->query($Command);
        if (!$result) {
            throw new DatabaseException($this->database->lastError());
        }

        $error_code = $result->errorCode();
        if ($error_code) {
            throw new DatabaseException($result->errorMessage(), $error_code);
        }

        $types = $this->getResultTypes($result);
        while ($pg_fetch_row = $result->fetchRow(null, PGSQL_ASSOC)) {
            $record = [];
            foreach ($pg_fetch_row as $name => $value) {
                if (!is_string($name)) {
                    continue;
                }

                $record[$name] = $this->getResultValue($types, $name, $value);
            }

            array_push($ret, $record);
        }

        return $ret;
    }

    /**
     * Retrieve query result types.
     *
     * @param PostgreSqlResultDTO $ResultSet The query result set
     * @return array<string,int>
     */
    private function getResultTypes(PostgreSqlResultDTO $ResultSet): array
    {
        $ret = [];
        $count = $ResultSet->numFields();

        for ($index = 0; $index < $count; $index++) {
            $name = $ResultSet->fieldName($index);
            $type = $ResultSet->fieldType($index);

            $ret[$name] = $this->getFieldType($type);
        }

        return $ret;
    }
}
