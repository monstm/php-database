<?php

namespace Samy\Database;

use mysqli_result;
use Samy\Database\Abstract\AbstractDatabase;
use Samy\Database\Constant\DatabaseFieldType;
use Samy\Database\DataTransferObject\MySqlDTO;
use Samy\Validation\ValidationException;

/**
 * Simple MySQL implementation.
 */
class MySql extends AbstractDatabase
{
    protected $field_types = [
        DatabaseFieldType::NULL => [
            MYSQLI_TYPE_NULL
        ],
        DatabaseFieldType::OBJECT => [
            MYSQLI_TYPE_JSON,
            MYSQLI_TYPE_ENUM,
            MYSQLI_TYPE_INTERVAL,
            MYSQLI_TYPE_SET,
            MYSQLI_TYPE_GEOMETRY
        ],
        DatabaseFieldType::BOOLEAN => [
            MYSQLI_TYPE_BIT
        ],
        DatabaseFieldType::INTEGER => [
            MYSQLI_TYPE_TINY,
            MYSQLI_TYPE_CHAR,
            MYSQLI_TYPE_SHORT,
            MYSQLI_TYPE_LONG,
            MYSQLI_TYPE_LONGLONG,
            MYSQLI_TYPE_INT24
        ],
        DatabaseFieldType::FLOAT => [
            MYSQLI_TYPE_DECIMAL,
            MYSQLI_TYPE_FLOAT,
            MYSQLI_TYPE_DOUBLE,
            MYSQLI_TYPE_NEWDECIMAL
        ],
        DatabaseFieldType::STRING => [
            MYSQLI_TYPE_TINY_BLOB,
            MYSQLI_TYPE_MEDIUM_BLOB,
            MYSQLI_TYPE_LONG_BLOB,
            MYSQLI_TYPE_BLOB,
            MYSQLI_TYPE_VAR_STRING,
            MYSQLI_TYPE_STRING
        ],
        DatabaseFieldType::TIME => [
            MYSQLI_TYPE_TIMESTAMP,
            MYSQLI_TYPE_DATE,
            MYSQLI_TYPE_TIME,
            MYSQLI_TYPE_DATETIME,
            MYSQLI_TYPE_YEAR,
            MYSQLI_TYPE_NEWDATE
        ]
    ];

    /**
     * Retrieve mysql database.
     *
     * @param array<string,mixed> $Data The data configuration.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return MySqlDTO
     */
    protected function databaseFactory(array $Data): MySqlDTO
    {
        return new MySqlDTO($Data);
    }

    /**
     * Retrieve mysql functions.
     *
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<string>
     */
    protected function functionsFactory(): array
    {
        $ret = ["IF"];
        $functions = array_map(
            function ($Data) {
                $name = (is_string($Data["name"]) ? $Data["name"] : "");
                $function = html_entity_decode(trim($name));
                $alphabet = preg_match("/[a-z]/i", $function);
                $no_space = !str_contains($function, " ");

                return ($alphabet && $no_space ? strtoupper($function) : "");
            },
            $this->query("HELP '%'")
        );

        foreach ($functions as $function) {
            if ($function == "") {
                continue;
            }

            array_push($ret, $function);
        }

        return array_unique($ret);
    }

    /**
     * Retrieve identifier name.
     *
     * @param string $UnidentifiedName The unidentified name.
     * @return string
     */
    protected function getIdentifier(string $UnidentifiedName): string
    {
        return "`" . $this->escape($UnidentifiedName) . "`";
    }

    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * Escapes special characters in the unescaped_string,
     * taking into account the current character set of the connection
     * so that it is safe to place it in a execute() or query().
     * If binary data is to be inserted, this function must be used.
     *
     * This function must always (with few exceptions) be used to make data safe before sending a query.
     *
     * @param string $UnescapedString The unescaped string.
     * @return string
     */
    public function escape(string $UnescapedString): string
    {
        return $this->database->realEscapeString($UnescapedString);
    }

    /**
     * Return an instance with executed query command.
     *
     * @param string $Command The sql command.
     * @throws DatabaseException If error.
     * @return static
     */
    public function execute(string $Command): self
    {
        $this->insert_id = 0;
        $this->affected_rows = 0;
        $this->executed = $Command;
        $this->database->query($Command);

        if ($this->database->errno()) {
            throw new DatabaseException($this->database->error(), $this->database->errno());
        }

        $this->insert_id = $this->database->insertId();
        $this->affected_rows = $this->database->affectedRows();

        return $this;
    }

    /**
     * Retrieve records from query command.
     *
     * @param string $Command The sql command.
     * @throws DatabaseException If error.
     * @return array<array<string,mixed>>
     */
    public function query(string $Command): array
    {
        $ret = [];
        $this->queried = $Command;
        $result = $this->database->query($Command);

        if (!$result) {
            throw new DatabaseException($this->database->error(), $this->database->errno());
        }

        $types = $this->getResultTypes($result);
        while ($fetch_assoc = $result->fetch_assoc()) {
            $record = [];
            foreach ($fetch_assoc as $name => $value) {
                if (!is_string($name)) {
                    continue;
                }

                $record[$name] = $this->getResultValue($types, $name, $value);
            }

            array_push($ret, $record);
        }

        $result->free();

        return $ret;
    }

    /**
     * Retrieve query result types.
     *
     * @param mysqli_result $ResultSet The query result set
     * @return array<string,int>
     */
    private function getResultTypes(mysqli_result $ResultSet): array
    {
        $ret = [];

        foreach ($ResultSet->fetch_fields() as $field) {
            if (!is_array($field)) {
                $field = get_object_vars($field);
            }

            $name = $field["name"] ?? "";
            $type = $field["type"] ?? 0;

            if (is_string($name)) {
                $ret[$name] = $this->getFieldType($type);
            }
        }

        return $ret;
    }
}
