<?php

namespace Samy\Database;

use Exception;

/**
 * Every Database exception MUST implement this interface.
 */
class DatabaseException extends Exception
{
}
