# Database

---

## Database Instance

Simple Database Implementation.

### MySQL Instance

Implementation MySQL instance.

```php
$config = [
    "host" => "localhost",
    "username" => "root",
    "password" => "",
    "database" => "database",
    "port" => 3306
];

$database = new \Samy\Database\MySql($config);
```

### PostgreSQL Instance

Implementation PostgreSQL instance.

```php
$config = [
    "host" => "localhost",
    "username" => "postgres",
    "password" => "postgres",
    "database" => "database",
    "port" => 5432
];

$database = new \Samy\Database\PostgreSql($config);
```

---

## Database Configuration

### MySQL Configuration

| Name     |  Type  |  Default  | Description                                            |
| -------- | :----: | :-------: | ------------------------------------------------------ |
| host     | string | localhost | Can be either a host name or an IP address.            |
| username | string |   root    | The MySQL user name.                                   |
| password | string |           | The MySQL password.                                    |
| database | string |           | The MySQL database to be used when performing queries. |
| port     |  int   |   3306    | The MySQL port number to be used to connect.           |

### PostgreSQL Configuration

| Name     |  Type  |  Default  | Description                                                 |
| -------- | :----: | :-------: | ----------------------------------------------------------- |
| host     | string | localhost | Can be either a host name or an IP address.                 |
| username | string | postgres  | The PostgreSQL user name.                                   |
| password | string | postgres  | The PostgreSQL password.                                    |
| database | string |           | The PostgreSQL database to be used when performing queries. |
| port     |  int   |   5432    | The PostgreSQL port number to be used to connect.           |

---

## Database Constants

### Field Type

| Name                       | Value |
| -------------------------- | :---: |
| DatabaseFieldType::UNKNOWN |   0   |
| DatabaseFieldType::BOOLEAN |   1   |
| DatabaseFieldType::INTEGER |   2   |
| DatabaseFieldType::FLOAT   |   3   |
| DatabaseFieldType::STRING  |   4   |
| DatabaseFieldType::TIME    |   5   |

### Condition

| Name                   | Value |
| ---------------------- | :---: |
| DatabaseCondition::AND |  AND  |
| DatabaseCondition::OR  |  OR   |

### Order

| Name                | Value |
| ------------------- | :---: |
| DatabaseOrder::ASC  |  ASC  |
| DatabaseOrder::DESC | DESC  |
