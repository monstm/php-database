# CRUD

---

## CRUD Interface

Describes CRUD interface.

### select

Retrieve multiple row records from query command.

```php
$data = [
    "table" => "table_name",
    "field" => [
        "id",
        "fullname" => "CONCAT(`first_name`, ' ', `last_name`)",
        "sex_type" => "gender"
    ],
    "filter" => [
        "gender" => "male",
        "`age` > 17",
    ],
    "group" => ["gender"],
    "order" => [
        "first_name" => DatabaseOrder::ASC,
        "age" => DatabaseOrder::DESC
    ],
    "limit" => 10,
    "offset" => 30,
    "sensitive" => true
];

$result = $database->select($data);
```

### record

Retrieve single row record from query command.

```php
$data = [
    "table" => "table_name",
    "field" => [
        "id",
        "fullname" => "CONCAT(`first_name`, ' ', `last_name`)",
        "sex_type" => "gender"
    ],
    "filter" => [
        "gender" => "male",
        "`age` > 17",
    ],
    "group" => ["gender"],
    "order" => [
        "first_name" => DatabaseOrder::ASC,
        "age" => DatabaseOrder::DESC
    ],
    "offset" => 30,
    "sensitive" => true
];

$record = $database->record($data);
```

### insert

Return an instance with inserted new record.

```php
$data = [
    "table" => "table_name",
    "data" => [
        "gender" => "female",
        "age" => 15,
        "first_name" => "jack",
        "last_name" => "lantern"
    ]
];

$database = $database->insert($data);
```

### update

Return an instance with updated record.

```php
$data = [
    "table" => "table_name",
    "data" => [
        "gender" => "female",
        "age" => 15,
        "first_name" => "CONCAT(`first_name`, ' ', `last_name`)",
        "last_name" => null
    ],
    "filter" => [
        "gender" => "male",
        "`age` > 17",
    ],
    "limit" => 2,
    "sensitive" => true
];

$database = $database->update($data);
```

### delete

Return an instance with deleted record.

```php
$data = [
    "table" => "table_name",
    "filter" => [
        "gender" => "male",
        "`age` > 17",
    ],
    "limit" => 2,
    "sensitive" => true
];

$database = $database->delete($data);
```

### count

Retrieve total existing record.

```php
$data = [
    "table" => "table_name",
    "filter" => [
        "gender" => "male",
        "`age` > 17",
    ],
    "sensitive" => true
];

$count = $database->count($data);
```
