# PHP Database

[
	![](https://badgen.net/packagist/v/samy/database/latest)
	![](https://badgen.net/packagist/license/samy/database)
	![](https://badgen.net/packagist/dt/samy/database)
	![](https://badgen.net/packagist/favers/samy/database)
](https://packagist.org/packages/samy/database)

This is a lean, consistent, and simple way to access databases.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/database
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-database>
* User Manual: <https://monstm.gitlab.io/php-database/>
* Documentation: <https://monstm.alwaysdata.net/php-database/>
* Issues: <https://gitlab.com/monstm/php-database/-/issues>
