# SQL

---

## SQL Interface

Describes SQL interface.

### escape

Escapes special characters in a string for use in an SQL statement.

```php
$escaped_string = $database->escape($unescaped_string);
```

### isFunction

Check if data expression is sql function.

```php
$is_function = $database->isFunction($expression);
```

### execute

Return an instance with executed query command.
Throw DatabaseException if error.

```php
$database = $database->execute($command);
```

### getInsertId

Retrieve the ID generated in the last execute() operation.

```php
$insert_id = $database->getInsertId();
```

### getAffectedRows

Retrieve number of affected rows in the last execute() operation.

```php
$affected_rows = $database->getAffectedRows();
```

### getExecuted

Retrieve the last command that was run in execute() operation.

```php
$executed = $database->getExecuted();
```

### query

Retrieve records from query command.
Throw DatabaseException if error.

```php
$records = $database->query($command);
```

### getQueried

Retrieve the last command that was run in query() operation.

```php
$queried = $database->getQueried();
```
