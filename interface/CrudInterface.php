<?php

namespace Samy\Database\Interface;

use Samy\Database\DatabaseException;
use Samy\Validation\ValidationException;

/**
 * Describes CRUD interface.
 */
interface CrudInterface
{
    /**
     * Retrieve multiple row records from query command.
     *
     * @param array<string,mixed> $Data The read data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<array<string,mixed>>
     */
    public function select(array $Data): array;

    /**
     * Retrieve single row record from query command.
     *
     * @param array<string,mixed> $Data The read data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<string,mixed>
     */
    public function record(array $Data): array;

    /**
     * Return an instance with inserted new record.
     *
     * @param array<string,mixed> $Data The create data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function insert(array $Data): self;

    /**
     * Return an instance with updated record.
     *
     * @param array<string,mixed> $Data The update data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function update(array $Data): self;

    /**
     * Return an instance with deleted record.
     *
     * @param array<string,mixed> $Data The delete data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function delete(array $Data): self;

    /**
     * Retrieve total existing record.
     *
     * @param array<string,mixed> $Data The count data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return int
     */
    public function count(array $Data): int;
}
