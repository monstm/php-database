<?php

namespace Samy\Database\Interface;

use Samy\Database\DatabaseException;

/**
 * Describes SQL interface.
 */
interface SqlInterface
{
    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * @param string $UnescapedString The unescaped string.
     * @return string
     */
    public function escape(string $UnescapedString): string;

    /**
     * Check if data expression is sql function.
     *
     * @param string $Expression The sql expression.
     * @return bool
     */
    public function isFunction(string $Expression): bool;

    /**
     * Return an instance with executed query command.
     *
     * @param string $Command The sql command.
     * @throws DatabaseException If error.
     * @return static
     */
    public function execute(string $Command): self;

    /**
     * Retrieve the ID generated in the last execute() operation.
     *
     * @return int
     */
    public function getInsertId(): int;

    /**
     * Retrieve number of affected rows in the last execute() operation.
     *
     * @return int
     */
    public function getAffectedRows(): int;

    /**
     * Retrieve the last command that was run in execute() operation.
     *
     * @return string
     */
    public function getExecuted(): string;

    /**
     * Retrieve records from query command.
     *
     * @param string $Command The sql command.
     * @throws DatabaseException If error.
     * @return array<array<string,mixed>>
     */
    public function query(string $Command): array;

    /**
     * Retrieve the last command that was run in query() operation.
     *
     * @return string
     */
    public function getQueried(): string;
}
