<?php

namespace Samy\Database\Abstract;

use Samy\Database\DatabaseException;
use Samy\Database\Constant\DatabaseCondition;
use Samy\Database\Constant\DatabaseFieldType;
use Samy\Database\Constant\DatabaseOrder;
use Samy\Database\DataTransferObject\CountDTO;
use Samy\Database\DataTransferObject\CreateDTO;
use Samy\Database\DataTransferObject\DeleteDTO;
use Samy\Database\DataTransferObject\ReadDTO;
use Samy\Database\DataTransferObject\UpdateDTO;
use Samy\Database\Interface\CrudInterface;
use Samy\Validation\ValidationException;

/**
 * This is a simple CRUD implementation that other CRUD can inherit from.
 */
abstract class AbstractCrud extends AbstractSql implements CrudInterface
{
    /** @var array<array<mixed>> */
    protected $field_types = [];

    /**
     * Retrieve identifier name.
     *
     * @param string $UnidentifiedName The unidentified name.
     * @return string
     */
    abstract protected function getIdentifier(string $UnidentifiedName): string;

    /**
     * Retrieve multiple row records from query command.
     *
     * @param array<string,mixed> $Data The read data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<array<string,mixed>>
     */
    public function select(array $Data): array
    {
        $read = new ReadDTO($Data);
        $select = $this->getSqlField($read->field());
        $table = $this->getSqlTable($read->table());
        $where = $this->getSqlFilter($read->filter(), $read->sensitive());
        $group_by = $this->getSqlGroup($read->group());
        $order_by = $this->getSqlOrder($read->order());
        $limit = $read->limit();
        $offset = $read->offset();

        return $this->query(
            "SELECT " . $select . " FROM " . $table .
                ($where != "" ? " WHERE " . $where : "") .
                ($group_by != "" ? " GROUP BY " . $group_by : "") .
                ($order_by != "" ? " ORDER BY " . $order_by : "") .
                ($limit > 0 ? " LIMIT " . $limit : "") .
                ($offset > 0 ? " OFFSET " . $offset : "")
        );
    }

    /**
     * Retrieve single row record from query command.
     *
     * @param array<string,mixed> $Read The read data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<string,mixed>
     */
    public function record(array $Read): array
    {
        $Read["limit"] = 1;
        $ret = $this->select($Read);

        return (count($ret) > 0 ? $ret[0] : []);
    }

    /**
     * Return an instance with inserted new record.
     *
     * @param array<string,mixed> $Data The create data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function insert(array $Data): self
    {
        $create = new CreateDTO($Data);
        $table = $this->getSqlTable($create->table());
        $insert = $this->getSqlInsert($create->data());

        return $this->execute("INSERT INTO " . $table . "(" . $insert["field"] . ") VALUES(" . $insert["value"] . ")");
    }

    /**
     * Return an instance with updated record.
     *
     * @param array<string,mixed> $Data The update data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function update(array $Data): self
    {
        $update = new UpdateDTO($Data);
        $table = $this->getSqlTable($update->table());
        $set = $this->getSqlSet($update->data());
        $where = $this->getSqlFilter($update->filter(), $update->sensitive());
        $limit = $update->limit();

        return $this->execute(
            "UPDATE " . $table . " SET " . $set .
                ($where != "" ? " WHERE " . $where : "") .
                ($limit > 0 ? " LIMIT " . $limit : "")
        );
    }

    /**
     * Return an instance with deleted record.
     *
     * @param array<string,mixed> $Data The delete data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function delete(array $Data): self
    {
        $delete = new DeleteDTO($Data);
        $table = $this->getSqlTable($delete->table());
        $where = $this->getSqlFilter($delete->filter(), $delete->sensitive());
        $limit = $delete->limit();

        return $this->execute(
            "DELETE FROM " . $table .
                ($where != "" ? " WHERE " . $where : "") .
                ($limit > 0 ? " LIMIT " . $limit : "")
        );
    }

    /**
     * Retrieve total existing record.
     *
     * @param array<string,mixed> $Data The count data.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return int
     */
    public function count(array $Data): int
    {
        $count = new CountDTO($Data);
        $select = "COUNT(0) AS " . $this->getIdentifier("count");
        $table = $this->getSqlTable($count->table());
        $where = $this->getSqlFilter($count->filter(), $count->sensitive());
        $query = "SELECT " . $select . " FROM " . $table . ($where != "" ? " WHERE " . $where : "");
        $record = $this->query($query);

        return intval($record[0]["count"] ?? 0);
    }

    /**
     * Retrieve SQL field statement.
     *
     * @param array<int|string,string> $Field The field data.
     * @return string
     */
    protected function getSqlField(array $Field): string
    {
        $ret = [];

        foreach ($Field as $alias => $expression) {
            $column = $this->isFunction($expression) ? $expression : $this->getIdentifier($expression);

            array_push($ret, $column . (is_string($alias) ? " AS " . $this->getIdentifier($alias) : ""));
        }

        return implode(", ", $ret);
    }

    /**
     * Retrieve SQL insert statements.
     *
     * @param array<string,mixed> $Data The data.
     * @return array<string,string>
     */
    protected function getSqlInsert(array $Data): array
    {
        $result_field = [];
        $result_value = [];

        foreach ($Data as $field => $value) {
            if (!is_string($field)) {
                continue;
            }

            array_push($result_field, $this->getIdentifier($field));
            array_push($result_value, $this->getFieldValue($value));
        }

        return [
            "field" => implode(", ", $result_field),
            "value" => implode(", ", $result_value)
        ];
    }

    /**
     * Retrieve SQL data statement.
     *
     * @param array<string,mixed> $Data The set data.
     * @return string
     */
    protected function getSqlSet(array $Data): string
    {
        $ret = [];

        foreach ($Data as $name => $data) {
            if (!is_string($name)) {
                continue;
            }

            $field = $this->getIdentifier($name);
            $value = $this->getFieldValue($data);
            array_push($ret, $field . " = " . $value);
        }

        return implode(", ", $ret);
    }

    /**
     * Retrieve SQL table statement.
     *
     * @param string|array<string> $Table The table data.
     * @return string
     */
    protected function getSqlTable(string|array $Table): string
    {
        $ret = [];

        if (is_string($Table)) {
            $Table = [$Table];
        }

        foreach ($Table as $name) {
            array_push($ret, $this->getIdentifier($name));
        }

        return implode(".", $ret);
    }

    /**
     * Retrieve SQL filter statement.
     *
     * @param array<mixed> $Filter The filter data.
     * @param bool $UseSensitiveCase Use sensitive-case operator.
     * @param bool $UseOrCondition Use OR condition.
     * @return string
     */
    protected function getSqlFilter(array $Filter, bool $UseSensitiveCase, bool $UseOrCondition = false): string
    {
        $ret = [];

        foreach ($Filter as $name => $data) {
            if (is_string($name)) {
                $field = $this->getIdentifier($name);
                $operant = $this->getFieldOperant($data, $UseSensitiveCase);
                $value = $this->getFieldValue($data);
                $result = $field . " " . $operant . " " . $value;
            } else {
                switch (gettype($data)) {
                    case "array":
                        $result = $this->getSqlFilter($data, $UseSensitiveCase, !$UseOrCondition);
                        break;
                    case "string":
                        $result = trim(strval($data));
                        break;
                    default:
                        $result = "";
                        break;
                }
            }

            if ($result != "") {
                array_push($ret, "(" . $result . ")");
            }
        }

        $condition = $UseOrCondition ? DatabaseCondition::OR : DatabaseCondition::AND;

        return trim(implode(" " . $condition . " ", $ret));
    }

    /**
     * Retrieve SQL group statement.
     *
     * @param array<string> $Group The group data.
     * @return string
     */
    protected function getSqlGroup(array $Group): string
    {
        $ret = [];

        foreach ($Group as $name) {
            array_push($ret, $this->getIdentifier($name));
        }

        return implode(", ", $ret);
    }

    /**
     * Retrieve SQL order statement.
     *
     * @param array<string,string> $Order The order data.
     * @return string
     */
    protected function getSqlOrder(array $Order): string
    {
        $ret = [];

        foreach ($Order as $name => $data) {
            $field = $this->getIdentifier($name);
            $value = (strtoupper($data) == DatabaseOrder::ASC ? "ASC" : "DESC");
            array_push($ret, $field . " " . $value);
        }

        return implode(", ", $ret);
    }

    /**
     * Retrieve field type.
     *
     * @param mixed $Type The database type
     * @return int
     */
    protected function getFieldType(mixed $Type): int
    {
        $ret = DatabaseFieldType::UNKNOWN;
        foreach ($this->field_types as $result => $data) {
            if (in_array($Type, $data)) {
                $ret = $result;
                break;
            }
        }

        if ($ret == DatabaseFieldType::UNKNOWN) {
            throw new DatabaseException("unknown type '" . $Type . "'");
        }

        return $ret;
    }

    /**
     * Retrieve field value.
     *
     * @param mixed $Value The value
     * @param bool $UseSensitiveCase Use sensitive-case operator.
     * @return string
     */
    protected function getFieldOperant(mixed $Value, bool $UseSensitiveCase): string
    {
        if (is_null($Value)) {
            return "IS";
        }

        return (!is_string($Value) || $this->isFunction($Value) || $UseSensitiveCase ? "=" : "LIKE");
    }

    /**
     * Retrieve field value.
     *
     * @param mixed $Value The value
     * @return string
     */
    protected function getFieldValue(mixed $Value): string
    {
        if (is_null($Value)) {
            return "NULL";
        }

        if (!is_string($Value)) {
            return strval($Value);
        }

        if ($this->isFunction($Value)) {
            return trim($Value);
        }

        return "'" . $this->escape($Value) . "'";
    }
}
