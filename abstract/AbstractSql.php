<?php

namespace Samy\Database\Abstract;

use Samy\Database\Interface\SqlInterface;

/**
 * This is a simple SQL implementation that other SQL can inherit from.
 */
abstract class AbstractSql implements SqlInterface
{
    /** @var MySqlDTO|PostgreSqlDTO|mixed */
    protected $database = null;

    /** @var array<string> */
    protected $functions = [];

    /** @var int */
    protected $insert_id = 0;

    /** @var int */
    protected $affected_rows = 0;

    /** @var string */
    protected $executed = "";

    /** @var string */
    protected $queried = "";

    /**
     * Check if data expression is sql function.
     *
     * @param string $Expression The sql expression.
     * @return bool
     */
    public function isFunction(string $Expression): bool
    {
        $ret = false;
        $trim = trim($Expression);

        if (($trim != "") && (substr($trim, -1) == ")")) {
            $explode = explode("(", $trim);
            $ret = in_array(strtoupper($explode[0]), $this->functions);
        }

        return $ret;
    }

    /**
     * Retrieve the ID generated in the last execute() operation.
     *
     * @return int
     */
    public function getInsertId(): int
    {
        return $this->insert_id;
    }

    /**
     * Retrieve number of affected rows in the last execute() operation.
     *
     * @return int
     */
    public function getAffectedRows(): int
    {
        return $this->affected_rows;
    }

    /**
     * Retrieve the last command that was run in execute() operation.
     *
     * @return string
     */
    public function getExecuted(): string
    {
        return $this->executed;
    }

    /**
     * Retrieve the last command that was run in query() operation.
     *
     * @return string
     */
    public function getQueried(): string
    {
        return $this->queried;
    }
}
