<?php

namespace Samy\Database\Abstract;

use Samy\Database\DatabaseException;
use Samy\Database\Constant\DatabaseFieldType;
use Samy\Validation\ValidationException;

/**
 * This is a simple Database implementation that other Database can inherit from.
 */
abstract class AbstractDatabase extends AbstractCrud
{
    /**
     * Retrieve database factory.
     *
     * @param array<string,mixed> $Config The database configuration.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return object
     */
    abstract protected function databaseFactory(array $Config): object;

    /**
     * Retrieve functions factory.
     *
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     * @return array<string>
     */
    abstract protected function functionsFactory(): array;

    /**
     * @param array<string,mixed> $Config The database configuration.
     * @throws DatabaseException If error.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Config = [])
    {
        $this->database = $Config["_database"] ?? $this->databaseFactory($Config);

        $functions = $Config["_functions"] ?? null;
        $this->functions = is_array($functions) ? $functions : $this->functionsFactory();
    }

    /**
     * Retrieve query result value.
     *
     * @param array<string,int> $Types Result types
     * @param string $Name Field name
     * @param mixed $Value Field value
     * @return mixed
     */
    protected function getResultValue(array $Types, string $Name, mixed $Value): mixed
    {
        if (!isset($Types[$Name]) || is_null($Value)) {
            return null;
        }

        switch ($Types[$Name]) {
            case DatabaseFieldType::NULL:
                $ret = null;
                break;
            case DatabaseFieldType::BOOLEAN:
                $ret = $Value ? true : false;
                break;
            case DatabaseFieldType::INTEGER:
                $ret = intval($Value);
                break;
            case DatabaseFieldType::FLOAT:
                $ret = floatval($Value);
                break;
            default:
                $ret = strval($Value);
                break;
        }

        return $ret;
    }
}
