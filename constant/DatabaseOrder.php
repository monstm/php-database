<?php

namespace Samy\Database\Constant;

/**
 * Simple Database Order implementation.
 */
class DatabaseOrder
{
    public const ASC    = "ASC";
    public const DESC   = "DESC";
}
