<?php

namespace Samy\Database\Constant;

/**
 * Simple Database Condition implementation.
 */
class DatabaseCondition
{
    public const AND    = "AND";
    public const OR     = "OR";
}
