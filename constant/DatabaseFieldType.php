<?php

namespace Samy\Database\Constant;

/**
 * Simple Database Field Type implementation.
 */
class DatabaseFieldType
{
    public const UNKNOWN    = 0;
    public const NULL       = 1;
    public const OBJECT     = 2;
    public const BOOLEAN    = 3;
    public const INTEGER    = 4;
    public const FLOAT      = 5;
    public const STRING     = 6;
    public const TIME       = 7;
}
